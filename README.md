### Radius proxy

[![N|Solid](https://www.juniper.net/assets/img/logos/juniper/juniper-networks-white-s.png)](https://www.juniper.net/us/en/)

#### What problem does the radius proxy application try to solve here?
The JUNOS BNG platform supports the radius VSA for HQOS/HCOS interface-set name.
This QoS interface-set name used to be configured statically in the Radius user database.
If an operator wants to reflect the line-id information (ACI/ARI) as part of the QoS interface-set VSA value, 
the Radius backend system needs to be enhanced to support dynamic QoS-interface-name value insertion logic with the line-id information.
To avoid a complex and expensive radius backend system upgrade, the simple JUNOS script app playing the radius proxy role 
at JUNOS BNG platform generates the QoS-interface-set name automatically with Line-ID information and insert newly composed value of 
the QoS-interface-set name into the Radius access response and accounting request message so that all access calls(PPPoE, DHCP) sharing the same line-id value & 
coming from multiple shared-VLAN interfaces are grouped under the same interface-set on which the QoS shaper is attached.
Again, new QOS interface-name VSA value derived from the access line information (ACI/ARI) presents in radius access response & accounting request message.

#### radius message support matrix
| code                             | supported? |
|----------------------------------|------------|
| access-request                   | yes        |
| accounting-request start         | yes        |
| accounting-interim start         | yes        |
| coa request                      | no         |
| disconnect request               | no         |

Please noticed following behaviors of the radius proxy app.
- If the access line-id information (ACI/ARI) does not exist in radius access message originated 
from the BNG radius client to the Radius proxy app, the QoS-interface-set value will not be inserted.
- If the radius access response message from the radius server includes the QoS-interface-set VSA, the radius proxy app will 
not replace value from the radius server with the access line-id information but just bypass the return message back to JUNOS BNG's radius client.


![Radius_proxy_role](radius_proxy_role.jpeg)

#### Input arguments format
The radius proxy application supports two options.
1. -t : timeout value for customer's radius server response. Default value is 3 seconds.
2. -m : radius proxy mapping entries. multiple entries can be specified with comma separator (',')
>   \<local ip>:\<local port>-<remote ip>:\<remote port>-\<radius secret>, ...
   
  The number of mapping entry is unlimited.
  You can copy jrp-qos.py and rename it and use it if you want to have application redundancy.
  For instance, 
  > jrp1.py for 1.1.1.1:11812-100.0.0.1:1812-juniper<br/>
  > jrp2.py for 1.1.1.1:11813-100.0.0.1:1813-juniper<br/>
  > jrp3.py for 1.1.1.1:21812-100.0.0.1:1812-juniper<br/>
  > jrp4.py for 1.1.1.1:21813-100.0.0.1:1813-juniper
  
  The radius secret key can be a <strong>plain string</strong> or <strong>JUNOS encrypted format</strong> value (starting with $9$ prefix)
  You can copy JUNOS encrypted secret key from radius-server configuration on Junos configuration mode CLI
 
#### installation steps
1. copy jrp-qos.py into MX BNG system.
2. register jrp-qos.py file into JET application with radius proxy mapping arguments.
3. add/update JUNOS radius client configuration pointing new radius proxy server listen ip and port. 

```sh
$ scp jrp-qos.py user@bng1:
$ ssh user@bng1
Warning: Permanently added 'petrel1.englab.juniper.net,10.9.46.227' (ECDSA) to the list of known hosts.
Last login: Fri Oct  2 06:04:55 2020 from 10.107.13.117
--- JUNOS 20.2R1.10 Kernel 64-bit  JNPR-11.0-20200608.0016468_buil
user@bng1> 
user@bng1> request system scripts refresh-from extension-service file jrp-qos.py url /var/home/user/ 
refreshing 'jrp-qos.py' from '/var/home/user/jrp-qos.py'
user@bng1> 
user@bng1> file list /var/db/scripts/jet/jrp-qos.py detail 
-rw-r-----  1 root  wheel       9283 Oct 2  16:48 /var/db/scripts/jet/jrp-qos.py
total files: 1

user@bng1>
user@bng1> edit 
Entering configuration mode
The configuration has been changed but not committed

[edit]
user@bng1# show system 
scripts {
    language python3;
}
extensions {
    providers {
        jnpr {
            license-type juniper deployment-scope commercial;
        }
    }
    extension-service {
        application {
            file jrp-qos.py {
                arguments "-m 1.1.1.1:1812-100.0.0.1:1812-$9$JfUi.QF/0BEP5BEcyW8ZUj,1.1.1.1:1813-100.0.0.1:1813-$9$JfUi.QF/0BEP5BEcyW8ZUj";
                daemonize;
                respawn-on-normal-exit;
                username root;
            }
        }
    }
}

[edit]
user@bng1# show interfaces lo0 
unit 0 {
    family inet {
        address 1.1.1.1/32;
    }
}

[edit]
user@bng1# show access radius-server 
1.1.1.1 {
    port 1812;
    accounting-port 1813;
    secret "$9$JfUi.QF/0BEP5BEcyW8ZUj"; ## SECRET-DATA
    timeout 1;
    retry 3;
    max-outstanding-requests 500;
    source-address 1.1.1.1;
}

[edit]
user@bng1# show access profile freeradius 
authentication-order radius;
radius {
    authentication-server 1.1.1.1;
    accounting-server 1.1.1.1;
}
accounting {
    order radius;
}

[edit]

```


#### radius message support matrix
| code                             | supported? |
|----------------------------------|------------|
| access-request                   | yes        |
| accounting-request start         | yes        |
| accounting-interim start         | yes        |
| coa request                      | no         |
| disconnect request               | no         |


#### freeradius server accounting data
```sh
Wed Oct  7 11:06:11 2020
        User-Name = "juniper1"
        Acct-Status-Type = Start
        Acct-Session-Id = "51"
        Event-Timestamp = "Oct  7 2020 11:05:00 EDT"
        Acct-Delay-Time = 0
        Service-Type = Framed-User
        Framed-Protocol = PPP
        ERX-Qos-Set-Name = "switch1-port1-onu1"
        ERX-Attr-177 = 0x41637475616c20526174653a2037303030306b
        Acct-Authentic = RADIUS
        ERX-Dhcp-Mac-Addr = "00aa.bb00.0101"
        Framed-IP-Address = 203.0.0.38
        Framed-IP-Netmask = 255.255.255.255
        NAS-Identifier = "petrel1"
        NAS-Port = 35
        NAS-Port-Type = Ethernet
        ERX-Virtual-Router-Name = "default:default"
        ERX-Pppoe-Description = "pppoe 00:aa:bb:00:01:01"
        ADSL-Agent-Circuit-Id = "switch1-port1"
        ADSL-Agent-Remote-Id = "onu1"
        ADSL-Forum-Attr-3 = 0x236f6c74312d706f6e31
        ERX-Attr-210 = 0x00000004
        NAS-IP-Address = 1.1.1.1
        Acct-Unique-Session-Id = "3eb16f21c6f94bae"
        Timestamp = 1602083171


```

#### software version
##### JUNOS
```sh
user@bng1# run show version 
Hostname: bng1
Model: mx480
Junos: 20.2R1.10
JUNOS OS Kernel 64-bit  [20200608.0016468_builder_stable_11]
JUNOS OS libs [20200608.0016468_builder_stable_11]
JUNOS OS runtime [20200608.0016468_builder_stable_11]
JUNOS OS time zone information [20200608.0016468_builder_stable_11]
JUNOS network stack and utilities [20200625.123713_builder_junos_202_r1]
JUNOS libs [20200625.123713_builder_junos_202_r1]
JUNOS OS libs compat32 [20200608.0016468_builder_stable_11]
JUNOS OS 32-bit compatibility [20200608.0016468_builder_stable_11]
JUNOS libs compat32 [20200625.123713_builder_junos_202_r1]
JUNOS runtime [20200625.123713_builder_junos_202_r1]
JUNOS sflow mx [20200625.123713_builder_junos_202_r1]
JUNOS py extensions2 [20200625.123713_builder_junos_202_r1]
JUNOS py extensions [20200625.123713_builder_junos_202_r1]
JUNOS py base2 [20200625.123713_builder_junos_202_r1]
JUNOS py base [20200625.123713_builder_junos_202_r1]
JUNOS OS crypto [20200608.0016468_builder_stable_11]
JUNOS na telemetry [20.2R1.10]
JUNOS Security Intelligence [20200625.123713_builder_junos_202_r1]
JUNOS mx libs compat32 [20200625.123713_builder_junos_202_r1]
JUNOS mx runtime [20200625.123713_builder_junos_202_r1]
JUNOS RPD Telemetry Application [20.2R1.10]
Redis [20200625.123713_builder_junos_202_r1]
JUNOS common platform support [20200625.123713_builder_junos_202_r1]
JUNOS Openconfig [20.2R1.10]
JUNOS mtx network modules [20200625.123713_builder_junos_202_r1]
JUNOS modules [20200625.123713_builder_junos_202_r1]
JUNOS mx modules [20200625.123713_builder_junos_202_r1]
JUNOS mx libs [20200625.123713_builder_junos_202_r1]
JUNOS SQL Sync Daemon [20200625.123713_builder_junos_202_r1]
JUNOS mtx Data Plane Crypto Support [20200625.123713_builder_junos_202_r1]
JUNOS daemons [20200625.123713_builder_junos_202_r1]
JUNOS mx daemons [20200625.123713_builder_junos_202_r1]
JUNOS appidd-mx application-identification daemon [20200625.123713_builder_junos_202_r1]
JUNOS Services URL Filter package [20200625.123713_builder_junos_202_r1]
JUNOS Services TLB Service PIC package [20200625.123713_builder_junos_202_r1]
JUNOS Services Telemetry [20200625.123713_builder_junos_202_r1]
JUNOS Services TCP-LOG [20200625.123713_builder_junos_202_r1]
JUNOS Services SSL [20200625.123713_builder_junos_202_r1]
JUNOS Services SOFTWIRE [20200625.123713_builder_junos_202_r1]
JUNOS Services Stateful Firewall [20200625.123713_builder_junos_202_r1]
JUNOS Services RTCOM [20200625.123713_builder_junos_202_r1]
JUNOS Services RPM [20200625.123713_builder_junos_202_r1]
JUNOS Services PCEF package [20200625.123713_builder_junos_202_r1]
JUNOS Services NAT [20200625.123713_builder_junos_202_r1]
JUNOS Services Mobile Subscriber Service Container package [20200625.123713_builder_junos_202_r1]
JUNOS Services MobileNext Software package [20200625.123713_builder_junos_202_r1]
JUNOS Services Logging Report Framework package [20200625.123713_builder_junos_202_r1]
JUNOS Services LL-PDF Container package [20200625.123713_builder_junos_202_r1]
JUNOS Services Jflow Container package [20200625.123713_builder_junos_202_r1]
JUNOS Services Deep Packet Inspection package [20200625.123713_builder_junos_202_r1]
JUNOS Services IPSec [20200625.123713_builder_junos_202_r1]
JUNOS Services IDS [20200625.123713_builder_junos_202_r1]
JUNOS IDP Services [20200625.123713_builder_junos_202_r1]
JUNOS Services HTTP Content Management package [20200625.123713_builder_junos_202_r1]
JUNOS Services Crypto [20200625.123713_builder_junos_202_r1]
JUNOS Services Captive Portal and Content Delivery Container package [20200625.123713_builder_junos_202_r1]
JUNOS Services COS [20200625.123713_builder_junos_202_r1]
JUNOS AppId Services [20200625.123713_builder_junos_202_r1]
JUNOS Services Application Level Gateways [20200625.123713_builder_junos_202_r1]
JUNOS Services AACL Container package [20200625.123713_builder_junos_202_r1]
JUNOS SDN Software Suite [20200625.123713_builder_junos_202_r1]
JUNOS Extension Toolkit [20200625.123713_builder_junos_202_r1]
JUNOS Packet Forwarding Engine Support (wrlinux9) [20200625.123713_builder_junos_202_r1]
JUNOS Packet Forwarding Engine Support (MXSPC3) [20.2R1.10]
JUNOS Packet Forwarding Engine Support (MX/EX92XX Common) [20200625.123713_builder_junos_202_r1]
JUNOS Packet Forwarding Engine Support (M/T Common) [20200625.123713_builder_junos_202_r1]
JUNOS Packet Forwarding Engine Support (aft) [20200625.123713_builder_junos_202_r1]
JUNOS Packet Forwarding Engine Support (MX Common) [20200625.123713_builder_junos_202_r1]
JUNOS Juniper Malware Removal Tool (JMRT) [1.0.0+20200625.123713_builder_junos_202_r1]
JUNOS J-Insight [20200625.123713_builder_junos_202_r1]
JUNOS jfirmware [20200625.123713_builder_junos_202_r1]
JUNOS Online Documentation [20200625.123713_builder_junos_202_r1]
JUNOS jail runtime [20200608.0016468_builder_stable_11]

```

##### Python
```sh
python3.7
```